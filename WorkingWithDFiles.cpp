#define _CRT_SECURE_NO_WARNINGS
#include "WorkingWithDFiles.h"
#include "word.h"
#include <fstream>

using namespace std;

void WorkingWithFiles::parseTextFile(const char* path, int& p)
{
    ifstream fin;
	fin.open(path);

	if (!fin.is_open())
	{
		cout << "\nCan't open file to write!";
		//return;
	}

	p = 0;
	const int m = 256;
	char tmp[m] = "";

	while (!fin.eof()) {
		fin.getline(tmp, m);
		p++;
	}

	//char** abcs = new char* [p];
	fin.seekg(0, fin.beg);

	for (int i = 0; i < p; i++)
	{
		fin.getline(tmp, m);
		int k = strlen(tmp);

		int count = 0;
		char** words = obtainWords(tmp, count);

		for (int j = 0; j < count; j++) {
			Word word(words[j]);
			ofstream streamOut;
			streamOut.open("text.bin", ios::binary);
			if (!streamOut.is_open())
			{
				cout << "\nCan't open file to write!";
				//return;
			}
			int bufSize = sizeof(Word);
			streamOut.write((char*)&word, bufSize);
			streamOut.close();
		}
		/*abcs[i] = new char[k + 1];
		strcpy(abcs[i], tmp);*/
	}

	fin.close();
}

char** WorkingWithFiles::obtainWords(char* source, int& n)
{
	const char* symbols = "ABCDEFGHIJKLMNOPQRSTUWVXYZabcdefghijklnmopqrstuwvxyz";
	char** words = new char* [strlen(source) / 2];

	n = 0;
	char* pword = strpbrk(source, symbols);
	while (pword)
	{
		int length = strspn(pword, symbols);
		words[n] = new char[length + 1];
		strncpy(words[n], pword, length);
		words[n][length] = '\0';
		pword += length;
		pword = strpbrk(pword, symbols);
		n++;
	}

	return words;
}
