#define _CRT_SECURE_NO_WARNINGS
#include "word.h"

Word::Word()
{
}

Word::Word(const char* src)
{
	this->setWord(src);
}

Word::~Word()
{
	delete[] word;
}

const int Word::getLength()
{
	return this->length;
}

const char* Word::getWord()
{
	return this->word;
}

Word* Word::createWords(char** words, int count)
{
	Word* array = new Word[count];

	for (int i = 0; i < count; i++)
	{
		array[i].setWord(words[i]);
	}

	return array;
}

void Word::setWord(const char* src)
{
	this->length = strlen(src);
	this->word = new char[this->length + 1];
	strcpy(this->word, src);
}
