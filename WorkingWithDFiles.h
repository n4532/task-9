#pragma once
class WorkingWithFiles
{
public:
	static void parseTextFile(const char*, int&);
	static char** obtainWords(char*, int&);
};

