#pragma once
#include <iostream>

class Word
{
public: 
	Word();
	Word(const char*);
	~Word();
	const int getLength();
	const char* getWord();
	static Word* createWords(char**, int);
private: 
	char* word{nullptr};
	int length{0};
	void setWord(const char*);
};

